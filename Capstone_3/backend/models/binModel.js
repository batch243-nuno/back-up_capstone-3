const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const binSchema = new Schema(
  {
    productID: {
      type: String,
    },
    name: {
      type: String,
      //   required: [true, 'Product name is required'],
    },
    description: {
      type: String,
      //   required: [true, 'Description is required'],
    },
    price: {
      type: Number,
      //   required: [true, 'Price is required'],
    },
    stocks: {
      type: Number,
      //   required: [true, 'Stock is required'],
    },
    isEmpty: {
      type: Boolean,
      default: false,
    },
    isActive: {
      type: Boolean,
      default: true,
    },
    createdAt: {
      type: String,
    },
    deletedOn: {
      type: String,
    },
    __v: {
      type: Number,
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model('Bin', binSchema);
