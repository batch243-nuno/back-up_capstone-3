import React from 'react';
import { Button, Modal } from 'react-bootstrap';
import { useEffect, useState } from 'react';
import { useProductsContext } from '../../hooks/useProductsContext';

function ProductStatus({ productID }) {
  //   console.log(productID);
  const [status, setStatus] = useState(
    `${
      productID.stocks ? (!productID.isActive ? 'Enable' : 'Disable') : 'Enable'
    }`
  );
  const { dispatch } = useProductsContext();

  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const updateStatusHandler = (event) => {
    setStatus(
      `${
        productID.stocks
          ? productID.isActive
            ? 'Enable'
            : 'Disable'
          : 'Enable'
      }`
    );
    event.preventDefault();
    const updateStatus = async () => {
      const response = await fetch(
        `${process.env.REACT_APP_API_URI}/products/updateProduct/${productID._id}`,
        {
          method: 'PATCH',
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
          },
        }
      );
      const json = await response.json();
      if (response.ok) {
        handleShow();
      }
    };
    updateStatus();
  };

  useEffect(() => {
    const fetchProducts = async () => {
      const response = await fetch(
        `${process.env.REACT_APP_API_URI}/products/allProducts`,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
          },
        }
      );
      const json = await response.json();
      if (response.ok) {
        dispatch({ type: 'SET_PRODUCTS', payload: json });
      }
    };
    fetchProducts();
  }, [show]);

  return (
    <>
      <Button onClick={updateStatusHandler} variant="warning">
        {status}
      </Button>
      <Modal show={show} onHide={handleClose} animation={false}>
        <Modal.Header closeButton>
          <Modal.Title>Product Status Updated</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {productID.name} is
          {productID.stocks ? (
            productID.isActive ? (
              <h1>Enabled</h1>
            ) : (
              <h1>Disabled</h1>
            )
          ) : (
            <>
              <h1>
                <strong>Zero</strong> in Stocks
              </h1>
              <h3>Cannot Activate</h3>
            </>
          )}
          !
        </Modal.Body>
      </Modal>
    </>
  );
}

export default ProductStatus;
