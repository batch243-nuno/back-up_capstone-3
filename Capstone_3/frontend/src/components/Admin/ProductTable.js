import { Table } from 'react-bootstrap';
import AddProductModal from '../Modals/AddProductModal';
import EditProductModal from '../Modals/EditProductModal';
import ProductStatus from './ProductStatus';
import RemoveProduct from './RemoveProduct';

function ProductTable({ products }) {
  // console.log(products);

  return (
    <>
      <AddProductModal />
      <Table striped bordered hover className="mt-5">
        <thead>
          <tr>
            <th>#</th>
            <th>Product Name</th>
            <th>Description</th>
            <th>Price</th>
            <th>Image File</th>
            <th>Stocks</th>
            <th>Status</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {products.map((product) => (
            <tr key={product._id}>
              <td>{}</td>
              <td>{product.name}</td>
              <td>{product.description}</td>
              <td>{product.price}</td>
              <td>iproduct.product.mgfile</td>
              <td>{product.stocks}</td>
              <td>
                {product.isActive && product.stocks > 0 ? 'Active' : 'Inactive'}
              </td>
              <td>
                <EditProductModal productID={product} />

                <ProductStatus productID={product} />
                <RemoveProduct productID={product} />
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </>
  );
}

export default ProductTable;
