import './css/Footer.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faFacebook,
  faInstagram,
  faTwitter,
  faGithub,
  faLinkedin,
} from '@fortawesome/free-brands-svg-icons';

function Footer() {
  return (
    <footer className=" d-flex flex-column text-white">
      <div className=" container border-bottom py-5 d-flex flex-md-row flex-column">
        <div className="newsletter col-md-5 col-12 mb-3 align-item-center">
          <img alt="logo" />
          <h3>Sign up for our newsletter and other informations</h3>
          <form id="newsForm" className="inputBox was-validated">
            <div className="d-flex flex-column flex-xl-row justify-content-between text-center text-lg-left">
              <div className="col-12 col-xl-8">
                <input
                  type="email"
                  className="form-control"
                  placeholder="Email"
                  autoComplete="true"
                  required
                />
              </div>
              <button
                className="btn btn-outline-success col-5 col-md-3 align-self-center mt-3 mt-xl-0"
                type="submit"
              >
                Subscribe
              </button>
            </div>
          </form>
          <div className="text-center mt-3">
            <p>For more Information, Please contact or visit this socials:</p>
            <a href="https://www.facebook.com/Rnjnnn/" target="_blank">
              <FontAwesomeIcon
                icon={faFacebook}
                className="btn btn-outline-success"
              ></FontAwesomeIcon>
            </a>
            <a href="https://www.instagram.com/jek_nuno/" target="_blank">
              <FontAwesomeIcon
                icon={faInstagram}
                className="btn btn-outline-success"
              ></FontAwesomeIcon>
            </a>
            <a href="https://twitter.com/Rnjnnn" target="_blank">
              <FontAwesomeIcon
                icon={faTwitter}
                className="btn btn-outline-success"
              ></FontAwesomeIcon>
            </a>
            <a href="https://github.com/ronjek" target="_blank">
              <FontAwesomeIcon
                icon={faGithub}
                className="btn btn-outline-success"
              ></FontAwesomeIcon>
            </a>
            <a href="https://www.linkedin.com/in/ronjeknuno/" target="_blank">
              <FontAwesomeIcon
                icon={faLinkedin}
                className="btn btn-outline-success"
              ></FontAwesomeIcon>
            </a>
          </div>
        </div>
        <div className="bakeryname col text-center mt-3">
          <h3>Panatasia Bakery</h3>
          <ul className="list-unstyled">
            <li>
              <a href="#" className="text-decoration-none">
                placeholder
              </a>
            </li>
            <li>
              <a href="#" className="text-decoration-none">
                placeholder
              </a>
            </li>
            <li>
              <a href="#" className="text-decoration-none">
                placeholder
              </a>
            </li>
            <li>
              <a href="#" className="text-decoration-none">
                placeholder
              </a>
            </li>
          </ul>
        </div>
        <div className="hmm col text-center mt-3">
          <h3>Pantasia Bakery</h3>
          <ul className="list-unstyled">
            <li>
              <a href="#" className="text-decoration-none">
                placeholder
              </a>
            </li>
            <li>
              <a href="#" className="text-decoration-none">
                placeholder
              </a>
            </li>
            <li>
              <a href="#" className="text-decoration-none">
                placeholder
              </a>
            </li>
            <li>
              <a href="#" className="text-decoration-none">
                placeholder
              </a>
            </li>
          </ul>
        </div>
      </div>
      <div className="d-flex justify-content-center">
        <div className="d-flex flex-column col-10 col-md-5 text-center mt-3 mt-md-5">
          <h5>
            Ⓡ 2022 The Perfect Loaf |{' '}
            <a href="#privacy" rel="nofollow">
              Privacy
            </a>{' '}
            |{' '}
            <a href="#accessibility" rel="nofollow">
              Accessibility
            </a>
          </h5>
          <h4 className="border-top">
            A <strong>AESTHETIC BREAD LOVER</strong>
          </h4>
        </div>
      </div>
    </footer>
  );
}

export default Footer;
