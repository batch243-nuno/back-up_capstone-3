import React, { useState } from 'react';
import { Form, Button, Modal } from 'react-bootstrap';
import Swal from 'sweetalert2';

import { useProductsContext } from '../../hooks/useProductsContext';

function AddProductModal() {
  const { dispatch } = useProductsContext();
  const [show, setShow] = useState(false);
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState(0);
  const [stocks, setStocks] = useState(0);
  const [products, setProducts] = useState(null);

  //   const history = useNavigate();

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const addProductHandler = (event) => {
    event.preventDefault();
    fetch(`${process.env.REACT_APP_API_URI}/products/addProduct`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
      },
      body: JSON.stringify({
        name,
        description,
        price,
        stocks,
      }),
    })
      .then((response) => response.json())
      .then((data) => {
        console.log(data);

        Swal.fire({
          title: 'Successfully Added!',
          icon: 'success',
          text: data.message,
        });
        setName('');
        setDescription('');
        setPrice('');
        setStocks('');
        setShow(false);
        dispatch({ type: 'CREATE_PRODUCTS', payload: data.result });
      });
  };

  return (
    <>
      <Button onClick={handleShow}>Add Product</Button>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Create Product</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form
            onSubmit={addProductHandler}
            className="col-12 px-3 was-invalidated "
          >
            <Form.Group className="">
              <Form.Label className="col-12">
                Bread Name
                <Form.Control
                  type="text"
                  placeholder="Name the Bread"
                  value={name}
                  onChange={(event) => setName(event.target.value)}
                  required
                />
              </Form.Label>
            </Form.Group>
            <Form.Group className="mb-2">
              <Form.Label>Bread Description</Form.Label>
              <Form.Control
                as="textarea"
                rows={3}
                placeholder="Describe the kind of Bread"
                value={description}
                onChange={(event) => setDescription(event.target.value)}
                required
              />
            </Form.Group>

            <Form.Group className="mb-5 d-flex flex-row align-items-center justify-content-between">
              <Form.Label className="col-5">
                Price
                <Form.Control
                  type="number"
                  placeholder="Enter Last Name"
                  value={price}
                  onChange={(event) => setPrice(event.target.value)}
                  required
                />
              </Form.Label>

              <Form.Label className="col-5">
                Stocks
                <Form.Control
                  type="number"
                  placeholder="Enter Last Name"
                  value={stocks}
                  onChange={(event) => setStocks(event.target.value)}
                  required
                />
              </Form.Label>
            </Form.Group>

            <Modal.Footer className="">
              <Button variant="secondary" onClick={handleClose}>
                Close
              </Button>
              <Button variant="primary" type="submit">
                Add Product
              </Button>
            </Modal.Footer>
          </Form>
        </Modal.Body>
      </Modal>
    </>
  );
}

export default AddProductModal;
