import { Container, Col, Card, Form, Button, Row } from 'react-bootstrap';
import './css/NavBar.css';

import ViewProductModal from './Modals/ViewProductModal';
import CheckedOutModal from './Modals/CheckOutModal';

function ProductContainer({ products }) {
  console.log(products);
  return (
    <Container>
      <Row>
        {products.map((product) => (
          <Col key={product._id} xs={12} lg={4} className="my-3 text-center">
            <Card className="p-3 h-100">
              <Card.Body className="d-flex flex-column">
                <Card.Title>
                  <strong>{product.name}</strong>
                </Card.Title>
                <Card.Text>
                  <strong>Description:</strong>
                  <br />
                  {product.description} <br />
                </Card.Text>
                <Col className="d-flex flex-column justify-content-end">
                  <Card.Text>
                    <strong>Price:</strong>
                    <br />
                    <strong>&#8369; {product.price}</strong>
                  </Card.Text>
                  <div className="d-flex justify-content-between">
                    <ViewProductModal productID={product} />
                    <CheckedOutModal productID={product} />
                  </div>
                </Col>
              </Card.Body>
            </Card>
          </Col>
        ))}
      </Row>
    </Container>
  );
}

export default ProductContainer;
