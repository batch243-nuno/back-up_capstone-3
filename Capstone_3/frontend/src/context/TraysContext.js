import { createContext, useReducer } from 'react';

export const TraysContext = createContext();

export const TraysReducer = (state, action) => {
  switch (action.type) {
    case 'SET_TRAYS':
      return {
        trays: action.payload,
      };
    case 'CREATE_TRAYS':
      return {
        trays: [...state.trays, action.payload],
      };
    default:
      return state;
  }
};
export const TraysContextProvider = ({ children }) => {
  const [state, trayDispatch] = useReducer(TraysReducer, {
    trays: null,
  });

  //   trayDispatch({ type: '', payload: '' });
  return (
    <TraysContext.Provider value={{ ...state, trayDispatch }}>
      {children}
    </TraysContext.Provider>
  );
};

export default TraysContext;
