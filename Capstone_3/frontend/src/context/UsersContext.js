import { createContext, useReducer } from 'react';

export const UsersContext = createContext();

export const usersReducer = (state, action) => {
  switch (action.type) {
    case 'SET_USERS':
      return {
        users: action.payload,
      };
    case 'CREATE_USERS':
      return {
        users: [...state.users, action.payload],
      };
    default:
      return state;
  }
};
export const UsersContextProvider = ({ children }) => {
  const [state, dispatch] = useReducer(usersReducer, {
    users: null,
  });

  //   dispatch({ type: '', payload: '' });
  return (
    <UsersContext.Provider value={{ ...state, dispatch }}>
      {children}
    </UsersContext.Provider>
  );
};

export default UsersContext;
