import React from 'react';
import FeaturedContainer from '../components/FeaturedContainer';
import Footer from '../components/Footer';
import Landing from '../components/Landing';
import NavBar from '../components/NavBar';

function Home() {
  return (
    <div>
      <NavBar />
      <Landing />
      <FeaturedContainer />
      <Footer />
    </div>
  );
}

export default Home;
